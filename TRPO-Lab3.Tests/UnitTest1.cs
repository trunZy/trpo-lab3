using NUnit.Framework;
using System;
using TRPO_Lab3.Lib;

namespace TRPO_Lab3.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        
        [Test]
        public void AddTest()
        {
            const int r1 = 1;
            const int r2 = 1;
            const int l = 1;
            const double s = 6.283;
            double delta = 0.01;
            var result = Konus.Area(r1, r2, l);
            Assert.AreEqual(s, result, delta);
        }
        [Test]
        public void AddTestOtricatel()
        {
            const int r1 = -1;
            const int r2 = 1;
            const int l = 1;

            Assert.Throws<ArgumentException>(()=> Konus.Area(r1, r2, l));
        }
        [Test]
        public void AddTestNaNull()
        {
            const int r1 = 4;
            const int r2 = 0;
            const int l = 1;


            Assert.Throws<ArgumentException>(() => Konus.Area(r1, r2, l));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPO_Lab3.Lib
{
    public static class Konus
    {
        public static double Area(double r1, double r2, double l)
        {
            ArgumentNullException.ThrowIfNull(r1, nameof(r1));
            ArgumentNullException.ThrowIfNull(r2, nameof(r2));
            ArgumentNullException.ThrowIfNull(l, nameof(l));

            if (r1 <= 0 || r2 <= 0 || l <= 0)
                throw new ArgumentException("all variables must be positive not null numbers");
            return Math.PI * (r1 + r2) * l;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TRPO_Lab3.Lib;

namespace TRPO_Lab3.UI.VM
{
    public class KonusViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private double _r1;

        public double R1
        {
            get => _r1;
            set {
                _r1 = value;
                OnPropertyChanged();
                if (R1 <= 0 || R2 <= 0 || L <= 0)
                    Result = 0 ;
                else
                    Result = Konus.Area(R1, R2, L);
            }
        }

        private double _r2;

        public double R2
        {
            get => _r2;
            set
            {
                _r2 = value;
                OnPropertyChanged();
                if (R1 <= 0 || R2 <= 0 || L <= 0)
                    Result = 0;
                else
                    Result = Konus.Area(R1, R2, L);
            }
        }

        private double _l;

        public double L
        {
            get => _l;
            set
            {
                _l = value;
                OnPropertyChanged();
                if (R1 <= 0 || R2 <= 0 || L <= 0)
                    Result = 0;
                else
                    Result = Konus.Area(R1, R2, L);
            }
        }

        private double _result;

        public double Result
        {
            get => _result;
            set 
            { 
                _result = value;
                OnPropertyChanged();
            }
        }

    }
}

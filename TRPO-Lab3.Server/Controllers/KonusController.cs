﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TRPO_Lab3.Lib;
using TRPO_Lab3.Server.Models;

namespace TRPO_Lab3.Server.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class KonusController : ControllerBase
    {
        [HttpPost]
        public JsonResult KonusArea([FromBody] KonusModel konus)
        {
            if (konus.R1 <= 0 || konus.R2 <= 0 || konus.L <= 0)
                return new JsonResult(0);
            return new JsonResult(Konus.Area(konus.R1, konus.R2, konus.L));
        }
    }
}

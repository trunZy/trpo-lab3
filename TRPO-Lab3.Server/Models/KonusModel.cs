﻿namespace TRPO_Lab3.Server.Models
{
    public class KonusModel
    {
        public double R1 { get; set; }
        public double R2 { get; set; }
        public double L { get; set; }
    }
}
